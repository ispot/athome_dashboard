import tkinter as tk
from tkinter import ttk
import sys, time, math, threading, csv, queue, serial, statistics, inspect, ctypes
from datetime import datetime
from tkinter.filedialog import asksaveasfilename
from tkinter.messagebox import askokcancel
import multiprocessing as mp
from queue import Empty
import serial.tools.list_ports
from optparse import OptionParser

awareness = ctypes.c_int()
errorCode = ctypes.windll.shcore.GetProcessDpiAwareness(0, ctypes.byref(awareness))
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)



class App(mp.Process):


    #https://stackoverflow.com/a/279586
    def static_vars( **kwargs):
        def decorate(func):
            for k in kwargs:
                setattr(func, k, kwargs[k])
            return func
        return decorate

    def __init__(self, *args):
        self.shared = args[0]
        self.reset = mp.Event()
        self.connectState = False
        super(App, self).__init__()

    def callback(self):
        self.shared["exit"].set()
        time.sleep(0.5)
        self.root.quit()
        #time.sleep(1)
        sys.exit(0)

    def setgain(self, gain):
        for g in self.gainb:
            g.config(background="white")
        self.shared["gain_conf"][0].put(gain)
        self.reset.set()

    def measure(self):
        self.reset.set()
        self.shared["req_adc"].set()

    def set_dark(self, *args):
        self.shared["dark_mode"].value = int(self.root.getvar(args[0]))

    def fillgraph(self, name):

        data_sums = [[-999,-999,-999,-999]]
        self.data_present = False
        self.alldatarows = []

        while 1:
            time.sleep(0.2)
            if(self.shared["exit"].is_set()):
                return
            
            if(self.reset.is_set()):
                data_sums = [[-999,-999,-999,-999]]
                self.reset.clear()

            if self.connectState == True and not self.shared["serial_connect"][0].is_set():
                    # device unplugged
                    self.connect_disconnect(forcedisconnect=True)


            try:
                self.gainb[self.shared["gain_conf"][1].get(False)].config(background="gray")
            except Empty:
                pass

            info = []
            try:
                while 1:
                    info = self.shared["serial_info"].get(False)
            except Empty:
                pass

            #for i,info in enumerate(info[:-2]):
            for i,info in enumerate(info):
                if i==2:
                    self.data_text[i].set("{}".format(["IDLE","REACTION 1","REACTION 2","MEASURE"][int(info)]))
                elif i==3:
                    self.data_text[i].set("{:.02f}".format(info/1000))
                else:
                    self.data_text[i].set("{:.02f}".format(info))

            flag = False
            try:
                while 1:
                    data_sums.append(self.shared["serial_data"].get(False))
                    flag = True
            except Empty:
                pass
            if flag:
                for i in range(len(data_sums[0])):
                    part = [x[i] for x in data_sums][1:]
                    avg = part[0]
                    conf = 0
                    if len(part) > 1:
                        avg = statistics.mean(part)
                        conf = statistics.stdev(part)
                    self.data_text2[i].set("{:.02f} ± {:>6.02f}".format(avg, conf))
                    self.lastdate = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    self.data_avg[i] = avg
                    self.data_confidence[i] = conf
                    self.data_present = True
                    self.add_button["state"] = "active"
                    self.data_text2[4].set(len(data_sums)-1)


    def clear(self):
        if not askokcancel(title=None, message="Clear all Data?"):
            return
        self.tree.delete(*self.tree.get_children())
        self.scount = 0
        self.alldatarows = []
        for i in self.data_text2:
            i.set("")
        self.data_name.set("")
        self.data_present = False
        self.add_button["state"] = "disabled"

    def add_data(self):
        self.data_present = False
        self.add_button["state"] = "disabled"

        e = [self.data_name.get(),str(self.lastdate)]
        e.extend(["{:.2f}".format(x) for x in sum(zip(self.data_avg,self.data_confidence),())])
        self.tree.insert(parent='', index='end', iid=self.scount, text=self.data_name.get(), values=e[1:])
        self.root.after(100,lambda:self.tree.yview_moveto(1))
        self.alldatarows.append(e)
        self.scount += 1
        for i in self.data_text2:
            i.set("")
        self.data_name.set("")
    
    def save_file(self):
        fname = asksaveasfilename()
        if fname != "":
            if fname[-4:] != ".csv":
                fname += ".csv"
            with open(fname, "w") as f:
                f.write(",".join(self.colnames)+"\n")
                print(self.alldatarows)
                print(self.colnames)
                f.writelines([",".join(x[0:len(self.colnames)])+"\n" for x in self.alldatarows])
             

    def resize_columns(self, x  ):
        full_w = self.tree.winfo_width()
        #fraction =  (0.16,0.12,0.12,0.06,0.12,0.06,0.12,0.06,0.12,0.06)
        fraction =  (0.20,0.20,0.15,0.15,0.15,0.15)
        for i in range(len(fraction)):
            self.tree.column("#{}".format(i), width=int(round(fraction[i]*full_w)))

    def populate_connections(self, *e, autoselect=False):
        ports = []
        for comport in serial.tools.list_ports.comports():
            if comport.vid in (0x0403, 0x10C4) and comport.pid in (0xEA60, 0xEA61, 0xEA62, 0xEA63, 0x6001):
                ports.append(comport.device)
        menu = self.devicesel["menu"]
        menu.delete(0, "end")
        for p in ports:
            menu.add_command(label=p, 
                             command=lambda value=p: self.connect_sel.set(value))
        if autoselect and len(ports) > 0:
            self.connect_sel.set(ports[0])
        if len(ports) == 1:
            self.connect_sel.set(ports[0])
        if len(ports) == 0:
            self.connect_sel.set("")

    def connect_disconnect(self, forcedisconnect=False):
        self.populate_connections()
        event = self.shared["serial_connect"][0]
        if self.connectState or forcedisconnect:
            event.clear()
            self.c_d_button.set("Connect")
            self.devicesel["state"] = "active"
            self.connectstate_name.set("Status: Disconnected")
            self.s.configure("cn.TLabel", foreground="Red")
            self.connectState = False
        else:
            # empty queues
            for i in self.shared["gain_conf"]:              
                try:
                    while 1:
                        i.get(False)
                except Empty:
                    pass
            self.shared["serial_connect"][1].value = self.connect_sel.get().encode("utf-8")
            event.set()
            self.c_d_button.set("Disconnect")
            self.devicesel["state"] = "disabled"
            self.connectstate_name.set("Status: Connected")
            self.s.configure("cn.TLabel", foreground="Green")
            self.connectState = True
            self.root.after(1000, lambda: self.carousel_grid() if self.shared["carousel"].value else self.carousel_w.grid_forget)

    def configure(self):
        win = tk.Toplevel()
        win.wm_title("Configuration")
        v = [tk.StringVar() for x in range(4)]
        v[0].set("63")
        v[1].set("30")
        v[2].set("95")
        v[3].set("5")

        win2 = tk.Frame(win)
        win2.pack(side="top", padx=30, pady=30)
        tk.Label(win2, text="Step 1:", font=("Helvetica",40)).pack(side="left", padx=(0,60))
        tk.Entry(win2, textvariable=v[0], font=("Helvetica",40), width=6).pack(side="left")
        tk.Label(win2, text="°C", font=("Helvetica",40)).pack(side="left", padx=(0,50))
        tk.Entry(win2, textvariable=v[1], font=("Helvetica",40), width=6).pack(side="left")
        tk.Label(win2, text="Minutes", font=("Helvetica",40)).pack(side="left")

        win3 = tk.Frame(win)
        win3.pack(side="top", padx=30, pady=30)
        tk.Label(win3, text="Step 2:", font=("Helvetica",40)).pack(side="left", padx=(0,60))
        tk.Entry(win3, textvariable=v[2], font=("Helvetica",40), width=6).pack(side="left")
        tk.Label(win3, text="°C", font=("Helvetica",40)).pack(side="left", padx=(0,50))
        tk.Entry(win3, textvariable=v[3], font=("Helvetica",40), width=6).pack(side="left")
        tk.Label(win3, text="Minutes", font=("Helvetica",40)).pack(side="left")

        tk.Button(win, text="Save", font=("Helvetica",40), command=win.destroy).pack(side="top", pady=10, ipadx=20)


    carousel_old = 1
    def carousel_set(self, *x):
        v = self.carousel_val.get()
        if v == "":
            v = self.carousel_old
        v = int(v)
        if v != self.carousel_old:
            self.shared["carousel_set"].put("${:02d}".format(v))
        self.carousel_old = v
        return v<49 and v>1

    def run(self):
        self.root = tk.Tk()
        self.root.title("SPOT Dashboard")
        self.root.iconbitmap('ispot.ico')
        self.scount = 0

        # hack to get things to have normal-ish scaling on low-res monitors
        if(ctypes.windll.user32.GetDpiForWindow(self.root.winfo_id()) < 200):
            self.root.tk.call('tk', 'scaling', 0.5)
        else:
            self.root.tk.call('tk', 'scaling', 1.0)

        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        self.top = tk.Frame(self.root)
        self.top.pack(fill=tk.X, pady=20, anchor=tk.N)

        self.middle = tk.Frame(self.root)
        self.middle.pack(pady=20)

        self.s = ttk.Style()
        self.s.configure('t.TButton', font=('Helvetica', 40))
        self.s.configure("TMenubutton", font=('Helvetica', 40), background="gray87")
        self.s.configure("TMenubutton.menu", font=('Helvetica', 40), background="gray87")
        self.s.configure("t.TLabel" , font=('Helvetica', 40))
        self.s.configure("cn.TLabel" , font=('Helvetica', 40), foreground="red")

        self.connect_sel = tk.StringVar(self.root, "UNKNOWN")

        self.connectframe = tk.Frame(self.middle)
        self.connectframe.pack(side=tk.LEFT, padx=60)
        self.connectstate_name = tk.StringVar(self.root, "Status: Disconnected")
        self.namelabel = ttk.Label(self.connectframe, textvariable=self.connectstate_name, style="cn.TLabel", anchor="center")
        self.namelabel.grid(row=0,column=0,columnspan=2, sticky="ew", pady=10)
        self.namelabel.grid_propagate(False)
        ttk.Label(self.connectframe, text="device:", style="t.TLabel").grid(row=1,column=0)
        self.devicesel = ttk.OptionMenu(self.connectframe, self.connect_sel)
        self.devicesel['menu'].configure(font=('Helvetica', 40))
        self.devicesel.grid(row=1,column=1,sticky="ew")
        self.devicesel.bind("<Button-1>", self.populate_connections)
        #self.devicesel.config(state="disabled")
        #self.root.nametowidget(self.devicesel.menuname).config( style="t.TMenubutton.q")
        #self.devicesel.config(style="t.TMenubutton") 
        self.c_d_button = tk.StringVar(self.root, "Connect")
        ttk.Button(self.connectframe, textvariable=self.c_d_button, style="t.TButton", command=self.connect_disconnect).grid(row=2,column=0,columnspan=2, sticky="ew", pady=10)
        
        self.root.update_idletasks() # updates widget width so minsize can be set
        tk.Grid.columnconfigure(self.connectframe, 0, weight=1, minsize= self.namelabel.winfo_width()//2+1)
        tk.Grid.columnconfigure(self.connectframe, 1, weight=1, minsize= self.namelabel.winfo_width()//2+1)

        self.populate_connections(autoselect=True)
        self.connect_disconnect()

        #self.stylename_elements_options(self.devicesel.winfo_class(), self.devicesel)

        self.buttonframe = tk.Frame(self.middle)
        self.buttonframe.pack(side=tk.LEFT)
        
        #tk.Button(self.buttonframe, text="Measure", font=("Courier",40,"bold"), command=self.measure ).grid(sticky="we", pady=20)
        self.darkmode = tk.IntVar()
        self.darkmode.trace("w", self.set_dark)
        tk.Checkbutton(self.buttonframe, text="Disable Illuminator", font=("Helvetica",40), variable=self.darkmode).grid(row=3,column=0,columnspan=5,pady=15)

        tk.Label(self.buttonframe, text="Amplifier gain:" , font=("Helvetica",40)).grid(row=1,column=0, columnspan=5)
        self.gainb = [0,0,0,0,0]
        self.gainb[0] = tk.Button(self.buttonframe, text="1X", font=("Courier",40,"bold"), background="white", command=lambda:self.setgain(0) )
        self.gainb[1] = tk.Button(self.buttonframe, text="2X", font=("Courier",40,"bold"), background="white", command=lambda:self.setgain(1) )
        self.gainb[2] = tk.Button(self.buttonframe, text="4X", font=("Courier",40,"bold"), background="white", command=lambda:self.setgain(2) )
        self.gainb[3] = tk.Button(self.buttonframe, text="8X", font=("Courier",40,"bold"), background="white", command=lambda:self.setgain(3) )
        self.gainb[4] = tk.Button(self.buttonframe, text="16X", font=("Courier",40,"bold"), background="white", command=lambda:self.setgain(4) )

        for i,g in enumerate(self.gainb):
            g.grid(row=2,column=i, sticky="we", ipadx=20)

        self.infoframe = tk.Frame(self.middle)
        self.infoframe.pack(side=tk.LEFT,padx=60)
        self.data_text = [tk.StringVar() for x in range(5)]
        for i,name in enumerate(("Temp:","Setpoint:","Status:","Time:","Voltage:")):
            tk.Label(self.infoframe, text=name, justify=tk.LEFT, anchor=tk.W, font=("Courier",40,"bold")).grid(row=i, sticky=tk.W)
            tk.Entry(self.infoframe, textvariable=self.data_text[i], font=("Courier",40,"bold"),  justify="right", \
                state="disabled", disabledforeground="black", disabledbackground="white").grid(row=i,column=1, sticky=tk.E)

        self.dataframe = tk.Frame(self.middle)
        self.dataframe.pack(side=tk.LEFT, padx=60)
        self.data_text2 = [tk.StringVar() for x in range(5)]

        tk.Label(self.top, text="SPOT System Data Dashboard v1.0.2", width=80, font=("Helvetica",42)).pack(side=tk.LEFT, pady=10, fill="x", expand=True)

        
        # ------
        self.meas_w = tk.Frame(self.root)
        self.meas_w.pack(fill="both", expand=True)

        self.data_avg = [tk.StringVar() for x in range(4)]
        self.data_confidence = [tk.StringVar() for x in range(4)]
        self.data_name = tk.StringVar()

        self.bframe2 = ttk.Frame(self.meas_w)
        self.bframe2.grid(row=0,column=1,columnspan=5, pady=30)

        self.carousel_w = ttk.Frame(self.bframe2)
        self.carousel_grid = lambda: self.carousel_w.grid(row=0, column=0, padx=(0,80))
        ttk.Label(self.carousel_w, text="Carousel:", anchor="e" , style="t.TLabel").grid(row=0,column=0, sticky="e")
        self.carousel_text = tk.StringVar()
        self.carousel_val = ttk.Spinbox(self.carousel_w, font=('Helvetica', 40), width=7, from_=1, to_=49, textvariable=self.carousel_text,validate="focus", validatecommand=self.carousel_set, command=self.carousel_set)
        self.carousel_val.bind("<Return>", self.carousel_set)
        self.carousel_val.grid(row=0, column=1)
        #self.carousel_text.trace("w", lambda *x: print(x))
        #ttk.Button(self.carousel_w, text="-", style="t.TButton", width=2).grid(row=0,column=1)
        #ttk.Entry(self.carousel_w, font=('Helvetica', 40), text="1", width=8).grid(row=0,column=2)
        #ttk.Button(self.carousel_w, text="+", style="t.TButton", width=2).grid(row=0,column=3)

        ttk.Button(self.bframe2, text="Measure", style="t.TButton", command=self.measure).grid(row=0,column=2, padx=10)
        self.add_button = ttk.Button(self.bframe2, text="Add", style="t.TButton", command=self.add_data, state="disabled")
        self.add_button.grid(row=0,column=3, padx=10)
        ttk.Button(self.bframe2, text="Export", style="t.TButton", command=self.save_file).grid(row=0,column=4, padx=10)
        ttk.Button(self.bframe2, text="Clear", style="t.TButton", command=self.clear ).grid(row=0,column=5, padx=10)
        ttk.Button(self.bframe2, text="Configure", style="t.TButton", command=self.configure ).grid(row=0,column=6, padx=10)
        tk.Grid.columnconfigure(self.bframe2, 1, weight=2)

        ttk.Label(self.meas_w, text="Name", anchor="center" , style="t.TLabel").grid(row=1,column=1, sticky="we")
        ttk.Label(self.meas_w, text="Channel 1", anchor="center", style="t.TLabel").grid(row=1,column=2, sticky="we")
        ttk.Label(self.meas_w, text="Channel 2", anchor="center", style="t.TLabel").grid(row=1,column=3, sticky="we")
        #ttk.Label(self.meas_w, text="Channel 3", anchor="center", style="t.TLabel").grid(row=1,column=4, sticky="we")
        #ttk.Label(self.meas_w, text="Channel 4", anchor="center", style="t.TLabel").grid(row=1,column=5, sticky="we")

        ttk.Entry(self.meas_w, font=('Helvetica', 40), textvariable=self.data_name).grid(row=2,column=1, sticky="we", padx=5)
        ttk.Entry(self.meas_w, font=('Helvetica', 40), state="disabled", textvariable=self.data_text2[0]).grid(row=2,column=2, sticky="we", padx=5)
        ttk.Entry(self.meas_w, font=('Helvetica', 40), state="disabled", textvariable=self.data_text2[1]).grid(row=2,column=3, sticky="we", padx=5)
        #ttk.Entry(self.meas_w, font=('Helvetica', 40), state="disabled", textvariable=self.data_text2[2]).grid(row=2,column=4, sticky="we", padx=5)
        #ttk.Entry(self.meas_w, font=('Helvetica', 40), state="disabled", textvariable=self.data_text2[3]).grid(row=2,column=5, sticky="we", padx=5)
        

        self.tree = ttk.Treeview(self.meas_w, columns=("",)*9)  

        verscrlbar = ttk.Scrollbar(self.meas_w,  
                           orient ="vertical",  
                           command = self.tree.yview) 
        verscrlbar.grid(row=3,column=7, sticky="nws")
        self.tree.configure(yscrollcommand=verscrlbar.set)

        self.s.configure('Treeview', rowheight=50,  font=('Helvetica', 30))  
        #self.colnames = ("Sample name","date","ch1","ch1 conf","ch2","ch2 conf","ch3","ch3 conf","ch4","ch4 conf")
        self.colnames = ("Sample name","date","ch1","ch1 stdev","ch2","ch2 stdev")
        for i in range(len(self.colnames)):
            self.tree.heading("#{}".format(i), text=self.colnames[i])
            self.tree.column("#{}".format(i), stretch=0, anchor="center",minwidth=0)


        self.tree.grid(row=3,column=0,columnspan=7, pady=30, padx=15, sticky="wens")
        self.s.configure("Treeview.Heading", font=('Helvetica', 40), background="#d6d6d6")
        tk.Grid.columnconfigure(self.meas_w, 0, weight=1)
        tk.Grid.columnconfigure(self.meas_w, 6, weight=1)
        tk.Grid.rowconfigure(self.meas_w, 3, weight=1)

        self.tree.bind("<Configure>",self.resize_columns)

        if (getattr(sys,"frozen", False) and __name__ == "__main__") or ((not getattr(sys,"frozen", False)) and __name__ == "__mp_main__"):
        #if __name__ == "__main__":
            graph_show = threading.Thread(target=self.fillgraph, args=(1,), daemon=True)
            graph_show.start()
            self.root.mainloop()

class Reader(mp.Process):
    def __init__(self, *args):
        self.shared = args[0]
        super(Reader, self).__init__()

    def run_connect(self, pname, connect_event):
        print("runconnect")
        self.shared["carousel"].value = False
        pname = pname.value.decode("utf-8")
        port = serial.Serial(pname, 115200, timeout=1)
        port.write("@@".encode("ascii"))
        while("init" not in port.readline().decode("ascii")):
            pass
        data_temp = []
        while 1:

            if not connect_event.is_set() or self.shared["exit"].is_set():
                if len(data_temp) > 0:
                    with open("temp_data.csv", "w") as f:
                        f.write("\n".join(data_temp))
                return
            line = port.readline()

            if not self.shared["gain_conf"][0].empty():
                channel = self.shared["gain_conf"][0].get()
                wr = "~{}\n".format(channel).encode("ascii")
                port.write(wr)

            if self.shared["req_adc"].is_set():
                port.write("!{}".format("." if self.shared["dark_mode"].value else "!").encode("ascii"))
                self.shared["req_adc"].clear()
            
            if not self.shared["carousel_set"].empty():
                port.write(self.shared["carousel_set"].get().encode("ascii"))

            try:
                data_raw = line.decode("ascii").strip("\r\n").split(",")
            except UnicodeDecodeError:
                pass

            if  data_raw[0] == "A":
                #data = np.asarray(data_raw[1:], dtype=np.float64)
                data = [float(x) for x in data_raw[1:]]
                #
                # remove comments to record temperature into temp_data.csv
                #
                #print(data[0])
                #data_temp.append("{},{}".format(time.perf_counter(),data[0]))
                #
                self.shared["serial_info"].put(data)
            elif data_raw[0] == "B":
                #data = np.asarray(data_raw[1:], dtype=np.float64)
                data = [float(x) for x in data_raw[1:]]
                self.shared["serial_data"].put(data)
            elif data_raw[0] == "C":
                p = ",".join(data_raw[1:]).split(":")
                print("{}:{}".format(p[0],p[1].split(",")))
                if p[0] == "adcgain":
                    self.shared["gain_conf"][1].put(int(p[1]))
                elif p[0] == "photo":
                    self.dualphoto = int(p[1]) > 1
                elif p[0] == "features":
                    if "2" in p[1].split(","):
                        self.shared["carousel"].value = True
                    else:
                        self.shared["carousel"].value = False
            else:
                print("Unknown data: {}".format(data_raw))


    def run(self):
        print("reader init.")
        event = self.shared["serial_connect"][0]
        while 1:

            if self.shared["exit"].is_set():
                    return
            while not event.wait(0.5):
                if self.shared["exit"].is_set():
                    return
            try:
                self.run_connect(self.shared["serial_connect"][1], event)
            except serial.serialutil.SerialException as e:
                event.clear()
                pass

if __name__ == "__main__":
    mp.freeze_support()

    _shared = {}
    _shared["serial_info"] = mp.Queue()
    _shared["serial_data"]  = mp.Queue()
    _shared["serial_connect"] = (mp.Event(), mp.Array("c", range(128)))
    _shared["gain_conf"] = (mp.Queue(),mp.Queue()) # (to AHDevice, from AHDevice)
    _shared["exit"] = mp.Event()
    _shared["req_adc"] = mp.Event()
    _shared["dark_mode"] = mp.Value("i",0)
    _shared["carousel"] = mp.Value("i",0)
    _shared["carousel_set"] = mp.Queue()

    p1 = App(_shared)
    p2 = Reader(_shared)
    
    [x.start() for x in (p1,p2)]
    _shared["exit"].wait()
    [x.close() for x in (_shared["serial_info"],_shared["serial_data"])]
    print("exiting.")

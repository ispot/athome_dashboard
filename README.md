# @home dashbord

The dashboard controls the at home iSPOT device over usb and allows for manual sensor measurements and debugging of the device.

## requirements

 - Python >= 3.7
 - pyserial, numpy, matplotlib, tkinter


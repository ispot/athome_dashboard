import serial, serial.tools.list_ports, sys, struct, random, os, subprocess, pickle
from datetime import datetime

os.chdir(sys.path[0]) # set path to script

def upload(bytedata):
    print("saving configuration...")
    port.write(bytedata)
    try:
        if bytes(bytedata) == port.read(24):
            print("Successfuly updated calibration.")
        else:
            print("Error writing configuration. Please try again.")
            sys.exit()
    except serial.SerialTimeoutException:
        print("Error communicating with device. Please try again.")
        sys.exit()

ports = [comport.device for comport in serial.tools.list_ports.comports()]
if len(ports) == 0:
    print("No devices found.")
    sys.exit()
else:
    print("Devices: {}".format(ports))

port = serial.Serial(ports[0], 115200, timeout=1)
port.write("@@".encode("ascii"))
line = ""
while("init" not in line):
    line = port.readline().decode("ascii")
params = {}
line = port.readline().decode("ascii")
while("C" == line[0]):
    a = line[2:].strip("\r\n").split(":")
    params[a[0]] = a[1]
    line = port.readline().decode("ascii")
print("firmware:{}".format(params["fw"]))
print("id:{}".format(params["id"]))
print("putting device into calibration mode.\n")

port.write("**".encode("ascii"))
line = ""
while("cal" not in line):
    line = port.readline().decode("ascii")

raw_bytes=bytes()
try:
    raw_bytes = port.read(48)
except serial.SerialTimeoutException:
    print("Error communicating with device. Please try again.")
    port.close()
    sys.exit()
raw_data = struct.unpack("<H3fH4HH3fH4H",raw_bytes)

# same data is sent twice
assert raw_data[:9] == raw_data[9:], "Error communicating with device. Please try again."

temp_raw_s = list(raw_data[1:4])
photo_raw_s = list(raw_data[5:9])

ih = bytearray(raw_bytes[:24])
counter = 0

prompt = input("Load calibration value from existing file? (Y/N)")
if "y" in prompt or "Y" in prompt:
    prompt = input("File path: ")
    while not os.path.isfile(prompt):
        print("Invalid path.")
        prompt = input("File path: ")
    saved_data = {}
    with open(prompt, "rb") as f:
        saved_data = pickle.load(f)
    print("--------")
    print("contents:{}".format(struct.unpack("<H3fH4H",saved_data["data"])))
    print("File was created on {}".format(saved_data["date"].ctime()))
    if saved_data["id"] != params["id"]:
        print("WARNING: device ID does not match calibration file!")
    if saved_data["fw"] != params["fw"]:
        print("WARNING: device firmware version does not match calibration file!")
    prompt = input("Proceed? (Y/N)")
    if not ("Y" in prompt or "y" in prompt):
        print("cancel.")
        sys.exit()
    upload(saved_data["data"])
    sys.exit()

print("Temperature Calibration:\n\tA: {}\n\tB: {}\n\tC: {}".format(*temp_raw_s))
print("Temperature Calibration Status:\n\t{}".format("OK" if int(params["tempcal"]) else "MISSING"))
print("Temperature Logic:\n\tT = A*X^2 + B*X + C")
print("--------")
print("Enter new temperature calibration values as comma-separated list, or ENTER to skip:")
a=input("?:").split(",")
if a[0] != "":
    assert len(a) == 3, "Needs 3 values."
    try:
        a = [float(i) for i in a]
    except ValueError:
        assert False, "Needs to be floating point."
    
    ih[counter] = 0xCD
    ih[counter+1] = 0xAB
    counter += 2
    for f in a:
        for i in struct.pack("<f",f):
            ih[counter] = i
            counter+= 1
    print("OK.")

print("--------")
print("Detection Calibration:\n\tA: {}\n\tB: {}\n\tC: {}\n\tD: {}".format(*photo_raw_s))
print("Detection Calibration Status:\n\t{}".format("OK" if int(params["photocal"]) else "MISSING"))
print("Detection logic:")
#  This is hardcoded.
print("\tch1<A AND ch2<B : NEGATIVE")
print("\tch2>C AND ch2>D : POSITIVE")
print("\tELSE            : INCONCLUSIVE")
print("--------")
print("Enter new detection calibration values as comma-separated list, or ENTER to skip:")
a=input("?:").split(",")
if a[0] != "":
    assert len(a) == 4, "Needs 4 values."
    try:
        a = [int(i) for i in a]
    except ValueError:
        assert False, "Needs to be integer."
    assert False not in [(i > 0 and i < 32767) for i in a], "values need to be between 0-32767."
    
    counter = 14
    ih[counter] = 0x34
    ih[counter+1] = 0x12
    counter += 2
    for f in a:
        for i in struct.pack("<H",f):
            ih[counter] = i
            counter+= 1
    print("OK.")

upload(ih)

port.close()
save = {}
save["date"] = datetime.now()
save["fw"] = params["fw"]
save["id"] = params["id"]
save["data"] = ih
fname = "calibration_{}.bin".format(params["id"][15:])
with open(fname, "wb") as f:
    pickle.dump(save, f)
print("A backup of the calibration file was saved to {}.".format(fname))
